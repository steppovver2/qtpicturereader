#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include <QtSql>

#include "image.h"

class Image;
struct ColorCounter;

/**
 * @brief The DbManager class
 * Класс который занимается полным циклом работой с БД
 * создает БД, ищет строки, добавляет, удаляет
 */
class DbManager : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief В конструкторе создается БД (если ее нет) и устанавливается соединение с ней
     * @param parent
     */
    explicit DbManager(QObject *parent = nullptr);
    /**
     * @brief Метод который добавляет в БД строку о картинке
     * Ключ -- автоинкремент, след столбец -- HASH
     * @param image изменяет поля Id у переданного объекта image
     * @return
     */
    bool addImage(QSharedPointer<Image> image) const;
    /**
     * @brief Возвращает ключ строки для существующей записи в БД с соотвествующим хешем
     * @param hash по переданному хешу возвращает id картинки в БД
     * @return
     */
    int selectImageId(const qint64 &hash) const;
    /**
     * @brief При передачи объекта класса Image, заполняет поля объекта в соответсвии
     * Данные берутся из БД
     * @param image заполняет в объект Image информацию о пикселях взятую из БД
     * @return
     */
    bool dataAboutImage(QSharedPointer<Image> image);
public slots:
    /**
     * @brief Слот который добавляет в БД информацию о всех пикселях для записи с переданным id
     * @param rgbHash константный указатель на QHash<QString, ColorCounter>
     * @param id идендификатор картинки в БД
     */
    void handleSaveDataAboutImage(const SharedHashPointer rgbHash, int id) const;
private:
    QSqlDatabase _picturesDB;
};

#endif // DBMANAGER_H
