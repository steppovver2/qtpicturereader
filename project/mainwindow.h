#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QSystemTrayIcon>

#include "qgraphicsscene.h"
#include "sortfilterproxymodel.h"
#include "image.h"

QT_BEGIN_NAMESPACE

class AboutWidget;
struct ColorCounter;
class GraphicsScene;
class Image;
class DbManager;
class MyModel;
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    /**
     * @brief LoadImage
     * Метод обрабатывающий файл переданный в виде пути до него
     */
    void loadImage(const QString &nFile);


protected:
    /**
     * @brief Виртуальный метод, который переопределяет действия при закрытии окна
     * Добавлена возможность сворачивания в трей при закрытии приложения
     * @param event
     */
    void closeEvent(QCloseEvent *event) override;

public slots:
    /**
     * @brief Click on the application icon in the system tray
     * @param reason This enum describes the reason the system tray was activated
     */
    void handleTrayIconActivated(QSystemTrayIcon::ActivationReason reason);
    /**
     * @brief Слот обрабатывающий данные о пикселях для передачи в графический интерфейс
     * @param rgbHash
     * @param isUpdatingTable true если нужно обновить данные в таблицу QtableView
     * @param isJson true если необходимо изменить состояние кнопки экспорта в состояние Enable
     */
    void handleDisplayPixelsInTableWidget(const SharedHashPointer rgbHash, bool isUpdatingTable, bool isJson);
    /**
     * @brief Слот вызывающий диалоговое окно для выбора картинки в проводнике
     */
    void handleSelectPictureInFileDialogClicked();

private slots:
    /**
     * @brief Слот фильтрующий таблицу данных
     * @param arg1 регулярное выражние
     */
    void handleFilterEditField(const QString &arg1);
    /**
     * @brief Слот обрабатывает нажатие кнопки export to json
     * Создает Json файл и записывает данные
     */
    void handleJsonExportClicked();
    /**
     * @brief Слот вызывающий QMessageBox с информацией о приложении
     */
    void handleAbout();
    void handlePaste();

private:
    void setupUI();
    void setupDB();
    void initTray();
    void createActions();
    void createMenus();

    AboutWidget* aboutWidget = nullptr;

    QMenu *_helpMenu = nullptr;
    QMenu *_settingsMenu = nullptr;
    QMenu *_editMenu = nullptr;
    QAction *_aboutAct = nullptr;
    QAction *_closeToTrayAct = nullptr;
    QAction *_pasteAct = nullptr;

    Ui::MainWindow *ui;
    QSharedPointer<Image> _image = nullptr;
    DbManager* _db = nullptr;
    GraphicsScene *_scene = nullptr;
    MyModel* _myModel = nullptr;
    SortFilterProxyModel *_proxyModel = nullptr;
    QSystemTrayIcon *_trayIcon = nullptr;

    QPixmap _pixmap;

};
#endif // MAINWINDOW_H
