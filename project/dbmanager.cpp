#include "dbmanager.h"

#include <QThread>


DbManager::DbManager(QObject *parent)
    : QObject{parent}
{
    _picturesDB = QSqlDatabase::addDatabase("QSQLITE");
    _picturesDB.setDatabaseName("im DB");

    if (!_picturesDB.open())
        qDebug() << "ERROR: connection with database failed";
    else
        qDebug() << "Database: connection ok";

    QSqlQuery query("CREATE TABLE IF NOT EXISTS pictures (id INTEGER PRIMARY KEY AUTOINCREMENT, hash int)");
    if(!query.isActive())
        qDebug() << "ERROR: " << query.lastError().text();

    auto sqlQuerry = "CREATE TABLE IF NOT EXISTS pixels (id INTEGER REFERENCES pictures (id), color TEXT, count INTEGER)";
    query.prepare(sqlQuerry);

    if (query.exec())
        qDebug() << "succcess create table pixels";
    else
        qDebug() << "ERROR: CREATE TABLE pixels: " << query.lastError();
}

bool DbManager::addImage(QSharedPointer<Image> image) const
{
    if (!image)
    {
        return false;
    }
    qint64 hash = image->hash();
    QSqlQuery query;
    query.prepare("INSERT INTO pictures (hash) VALUES (:hash)");
    query.bindValue(":hash", hash);
    auto success = query.exec();
    if (!success)
    {
        qDebug() << "addImage error:" << query.lastError();
        return success;
    }

    image->setId(selectImageId(hash));
    return success;
    }

void DbManager::handleSaveDataAboutImage(const SharedHashPointer rgbHash, int id) const
{
    QSqlQuery query;
    QString str;

    int i = 0;
    for (auto it = rgbHash->constBegin(); it != rgbHash->constEnd(); ++it, ++i)
    {
        if (i == 0)
        {
            str = QString("INSERT INTO pixels (id, color, count) VALUES");
        }
        str += QString(" ('%1', '%2', '%3')").arg(id).arg(it.key()).arg(it.value().count);
        if (i == 1000)
        {
            i = -1;
            query.prepare(str);
            if (!query.exec())
                qCritical() << "error while inserting data" << query.lastError();
        }
        else
        {
            str += ",";
        }
    }
    if (str.size() > 0)
    {
        str.chop(1);
        query.prepare(str);
        if (!query.exec())
            qCritical() << "error while inserting data" << query.lastError();
    }
}

bool DbManager::dataAboutImage(QSharedPointer<Image> image)
{
    auto tmp = SharedHashPointer(new QHash<QString, ColorCounter>);
    QSqlQuery query;
    auto sqlQuerry = QString("SELECT color, count FROM pixels WHERE id = :id");
    query.prepare(sqlQuerry);
    query.bindValue(":id", image->id());

    auto success = query.exec();
    if (success)
    {
        while (query.next())
        {
            ColorCounter colorCounter;
            colorCounter.count = query.value(1).toInt();
            tmp->insert(query.value(0).toString(), colorCounter);
        }
        image->setRGBHash(tmp);
    }
    else
    {
        qCritical() << "ERROR: SELECT pixels: " << query.lastError();
    }
    return success;
}

int DbManager::selectImageId(const qint64 &hash) const
{
    QSqlQuery query;
    query.prepare("SELECT id FROM pictures WHERE hash = (:hash)");
    query.bindValue(":hash", hash);

    if (query.exec() && query.next())
    {
        int id = query.value(0).toInt();
        return id;
    }
    qCritical() << Q_FUNC_INFO;
    return 0;
}


