#ifndef MYMODEL_H
#define MYMODEL_H

#include <QAbstractTableModel>
#include <image.h>

class MyModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit MyModel(const QHash <QString, ColorCounter> rgbHash, QObject *parent = nullptr);
    ~MyModel();


public:
    /**
     * @brief Возвращает количество строк необходимое для отображения данных в виде таблицы
     * @param parent
     * @return
     */
    int rowCount(const QModelIndex &index) const override;
    /**
     * @brief Возвращает количество столбцов необходимое для отображения данных в виде таблицы
     * @param parent
     * @return
     */
    int columnCount(const QModelIndex &index) const override;
    /**
     * @brief Перегруженный метод для отображения данных в таблице
     * Вызывается только для тех ячеек, что видит пользователь
     * @param index
     * @param role
     * @return Возвращает данные, которые должны отобразиться в ячейке
     */
    QVariant data(const QModelIndex &index, int role) const override;
    /**
     * @brief Возвращает данные которые должны отображаться в заголовках соответсвующих строк/столбцов
     * @param section
     * @param orientation
     * @param role
     * @return
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
private:
    QHash <QString, int> *_rgbHash = nullptr;
};

#endif // MYMODEL_H
