QT += core gui
QT += sql
QT += concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

unix {
    TARGET_SYSTEM = "linux"
} else: {
    TARGET_SYSTEM = "win"
}

OUTPUT_DIR = "$$PWD/../build/$$TARGET_SYSTEM/$$BUILD_TYPE"

DESTDIR = "$$OUTPUT_DIR/bin/"
OBJECTS_DIR = "$$OUTPUT_DIR/obj/"
MOC_DIR = "$$OUTPUT_DIR/moc/"
RCC_DIR = "$$OUTPUT_DIR/res/"
UI_DIR = "$$OUTPUT_DIR/ui/"

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#DEFINES +=

SOURCES += \
    aboutwidget.cpp \
    dbmanager.cpp \
    graphicsscene.cpp \
    graphicsview.cpp \
    image.cpp \
    jsonconverter.cpp \
    main.cpp \
    mainwindow.cpp \
    mymodel.cpp \
    sortfilterproxymodel.cpp

HEADERS += \
    aboutwidget.h \
    dbmanager.h \
    graphicsscene.h \
    graphicsview.h \
    image.h \
    jsonconverter.h \
    mainwindow.h \
    mymodel.h \
    sortfilterproxymodel.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    source.qrc
