#include "graphicsscene.h"
#include "dbmanager.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tablewidget.h"
#include "mymodel.h"
#include "jsonconverter.h"
#include "aboutwidget.h"

#include <QFileDialog>
#include <QDebug>
#include <QHash>
#include <QtConcurrent>
#include <QTableView>
#include <QHeaderView>
#include <QStyle>
#include <QMessageBox>
#include <QGraphicsTextItem>
#include <QClipboard>

#define ICON_PATH ":/icons/images/icon.png"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    initTray();

    // THREAD
    qRegisterMetaType<SharedHashPointer>();
    qDebug() << QThread::currentThread() << Q_FUNC_INFO;

    setupUI();
    setupDB();

    // MenuBar
    createActions();
    createMenus();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(this->isVisible() && _closeToTrayAct->isChecked()){
        _trayIcon->show();
        event->ignore();
        this->hide();

        QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information);
        _trayIcon->showMessage("Tray Program",
                               tr("The application is minimized to tray."
                                  "\nIn order to maximaize the application window, click on the"
                                  "application icon in system tray."),
                               icon,
                               100);
    }
}

void MainWindow::setupUI()
{
    ui->setupUi(this);

    this->setWindowTitle("Picture Reader");
    this->setWindowIcon(QIcon(ICON_PATH));

    _scene = new GraphicsScene(this, this);
    ui->graphicsView->setScene(_scene);

    _proxyModel = new SortFilterProxyModel(this);
    ui->tableView->setModel(_proxyModel);
}

void MainWindow::setupDB()
{
    _db = new DbManager(this);

    ui->JsonExportButton->setEnabled(false);

    connect(ui->JsonExportButton, &QPushButton::clicked,
            this, &MainWindow::handleJsonExportClicked);
    connect(ui->FilterLineEdit, &QLineEdit::textEdited,
            this, &MainWindow::handleFilterEditField);
}

void MainWindow::initTray()
{
    _trayIcon = new QSystemTrayIcon(this);
    _trayIcon->setIcon(QIcon(":/icons/images/icon.png"));
    _trayIcon->setToolTip("PictureReader" "\n"
                         "Program");

    QMenu *menu = new QMenu(this);
    QAction *viewWindow = new QAction(tr("Open PictureReader"), this);
    QAction *quitAction = new QAction(tr("Quit PictureReader"), this);

    connect(viewWindow, &QAction::triggered,
            this, &MainWindow::show);
    connect(quitAction, &QAction::triggered,
            this, &MainWindow::close);

    menu->addAction(viewWindow);
    menu->addAction(quitAction);

    _trayIcon->setContextMenu(menu);

    connect(_trayIcon, &QSystemTrayIcon::activated,
                this, &MainWindow::handleTrayIconActivated);
}

void MainWindow::handleTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (QSystemTrayIcon::Trigger == reason && _closeToTrayAct->isChecked() && !isVisible())
    {
        show();
    }
    _trayIcon->hide();
}

void MainWindow::handleDisplayPixelsInTableWidget(const SharedHashPointer rgbHash,
                                                  bool isUpdatingTable, bool isJson)
{

    if (!rgbHash) {
        return;
    }
    if (isJson)
    {
        ui->JsonExportButton->setEnabled(true);
        ui->statusbar->clearMessage();
        _scene->setEnabledDoubleClick(true);
    }

    if (!isUpdatingTable)
        return;


    ui->tableView->clearSpans();
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//    ui->tableView->setStyleSheet("QHeaderView::section { "
//                                 "background-color: #ffffff;"
//                                 "border-width: 2px;"
//                                 "border-color: #4a4a4a;"
//                                 "border-style: solid ;}"

//                                 "QTableCornerButton::section {"
//                                 "background-color: #edfbff;"
//                                 "border-color: beige;"
//                                 "border-width: 1px;}");

    if (_myModel) {
        delete _myModel;
    }

    _myModel = new MyModel(*rgbHash, this);

    _proxyModel->setSourceModel(_myModel);

    ui->tableView->horizontalHeader()->setSortIndicator(-1, Qt::AscendingOrder);
    ui->tableView->setSortingEnabled(true);
}

void MainWindow::handleSelectPictureInFileDialogClicked()
{
    QString filename = QFileDialog::getOpenFileName(
                this, tr("Choose picture"), QString(), "Images (*.png *.xpm *.jpg)");
    if (!filename.isEmpty()) {
        loadImage(filename);

    }
}

void MainWindow::handleFilterEditField(const QString &arg1)
{
    QRegExp rx(arg1, Qt::CaseSensitive, QRegExp::Wildcard);
    _proxyModel->setFilterRegExp(rx);
    _proxyModel->setFilterKeyColumn(0);
}

void MainWindow::handleJsonExportClicked()
{
    if (_image) {
        JsonConverter json;
        json.saveToJsonFile(_image);
    }
}

void MainWindow::handleAbout()
{
    aboutWidget = new AboutWidget(QPixmap(ICON_PATH));
}

void MainWindow::handlePaste()
{
    const QClipboard *clipboard = QApplication::clipboard();
    const QMimeData *mimeData = clipboard->mimeData();

    if (mimeData->hasImage())
    {
    _pixmap = qvariant_cast<QPixmap>(mimeData->imageData());
    loadImage("");
    }
}

void MainWindow::createActions()
{
    _aboutAct = new QAction(tr("&About"), this);
    _aboutAct->setStatusTip(tr("About PictureReader"));
    connect(_aboutAct, &QAction::triggered,
            this, &MainWindow::handleAbout);

    _closeToTrayAct = new QAction(tr("&Close To Tray"), this);
    _closeToTrayAct->setStatusTip(tr("\"Minimize to Tray\" option"));
    _closeToTrayAct->setCheckable(true);

    _pasteAct = new QAction(tr("&Paste"), this);
    _pasteAct->setShortcuts(QKeySequence::Paste);
    _pasteAct->setStatusTip(tr("Paste the picture from Clipboard"));
    connect(_pasteAct, &QAction::triggered,
            this, &MainWindow::handlePaste);
}

void MainWindow::createMenus()
{

    _editMenu = menuBar()->addMenu(tr("&Edit"));
    _editMenu->addAction(_pasteAct);

    _settingsMenu = menuBar()->addMenu(tr("&Settings"));
    _settingsMenu->addAction(_closeToTrayAct);

    _helpMenu = menuBar()->addMenu(tr("&Help"));
    _helpMenu->addAction(_aboutAct);
}

void MainWindow::loadImage(const QString &nFile = QString())
{
    if (!nFile.isEmpty())
        _pixmap.load(nFile);

    ui->graphicsView->setEnabledText(false);
    _scene->clear();
    _scene->addPixmap(_pixmap);
    ui->graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    _scene->setSceneRect(_pixmap.rect());

    ui->JsonExportButton->setEnabled(false);
    _scene->setEnabledDoubleClick(false);

    _image = QSharedPointer<Image>(new Image(this, &_pixmap, _db));

    if (!_image->id())
    {
        auto success = _db->addImage(_image);
        if (!success)
        {
            ui->statusbar->showMessage(tr("Error while adding image"));
        }

        QtConcurrent::run(_image.data(), &Image::countColors, success, true);
    }
    else
    {
        if (_db->dataAboutImage(_image))
            QtConcurrent::run(_image.data(), &Image::countColors, false, false);
        else
            QtConcurrent::run(_image.data(), &Image::countColors, false, true);
    }

    ui->statusbar->showMessage(tr("Calculating... Please wait"));

    qDebug() << QThread::currentThread();
}
