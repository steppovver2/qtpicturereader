#ifndef IMAGE_H
#define IMAGE_H

#include <QList>
#include <QCryptographicHash>
#include <QSharedPointer>

struct ColorCounter;
typedef QSharedPointer<QHash <QString, ColorCounter>> SharedHashPointer;

class DbManager;
class MainWindow;

struct ColorCounter
{
    int count = 0;
    QList<QPair<int, int>> listOfCoordinates;

};

Q_DECLARE_METATYPE(SharedHashPointer)

/**
 * @brief The Image class
 * Класс отвечающий за работу с картинками
 */
class Image : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Настройка сигнал -- слот, Присваивание хеш суммы
     * @param parent
     * @param filename
     * @param db
     */
    explicit Image(MainWindow *parent = nullptr, const QPixmap *pixmap = nullptr, DbManager *db = nullptr);
    ~Image();
    /**
     * @brief getter для поля _hash
     * @return
     */
    qint64 hash() const;
    /**
     * @brief setter для поля _id
     * @param id
     */
    void setId (int id);
    /**
     * @brief getter для поля _id
     * @return
     */
    int id() const;
    /**
     * @brief Метод расчитывающий количетсво пикселей разных цветов
     * @param isSaving
     * @param isDisplay
     */
    void countColors(bool isSaving, bool isDisplay);
    /**
     * @brief setter для поля _rgbMap
     * @param rgbHash
     */
    void setRGBHash(SharedHashPointer rgbHash);
    /**
     * @brief Метод возвращает содержимое поля _rgbHash
     * @return
     */
    const SharedHashPointer getRGBHash() const;

signals:
    void needToDisplayPixels(const SharedHashPointer _rgbHash, bool isUpdatingTable, bool isJson);
    void needToSavePixels(const SharedHashPointer _rgbHash, int id);

private:
    QImage* _img = nullptr;
    SharedHashPointer _rgbHash = nullptr;
    MainWindow *_parent = nullptr;
    qint64 _hash;
    int _id = 0;
};

#endif // IMAGE_H
