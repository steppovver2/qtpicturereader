#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H
#include <QGraphicsScene>
#include <QFileInfo>


class MainWindow;
/**
 * @brief The GraphicsScene class
 * Класс для визуализации картинки
 */
class GraphicsScene : public QGraphicsScene
{
public:
    explicit GraphicsScene(QObject *parent = nullptr, MainWindow *window = nullptr);
    /**
     * @brief This property holds whether the double click is enabled
     * @param enable By default, this property is true
     */
    void setEnabledDoubleClick(bool enable = true);
protected:
    /**
     * @brief Проверяет файл которые перенесли методом drag and drop на соответсвие с фильтром
     * Разрешенные форматы, PNG jpg
     * @param event
     */
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
    /**
     * @brief переопредлен метод, чтобы не запрещал drop
     * @param event
     */
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    /**
     * @brief При бросании файла вызывает метод загрузки у объекта класса MainWindow
     * @param event
     */
    void dropEvent(QGraphicsSceneDragDropEvent *event) override;
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;

private:
QFileInfo fileName = QFileInfo();
MainWindow* _mainWindow = nullptr;
QList<QString> _fileFormatList {"png", "jpg"};
bool _isDoubleClickEnabled = true;
};


#endif // GRAPHICSSCENE_H
