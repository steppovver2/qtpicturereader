#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>
#include <QLabel>
/**
 * @brief The GraphicsView class
 * Виджет для визульного взаимодейсвтия с картинкой
 */
class GraphicsView : public QGraphicsView
{
public:
    /**
     * @brief В конструкторе выствляется параметр setdragmode(true)
     * @param parent
     */
    GraphicsView(QWidget *parent = nullptr);
    /**
     * @brief Method changed the Qlabel's isEnabled parameter
     * @param enable By default, this property is true.
     */
    void setEnabledText(bool enable);
protected slots:
    /**
     * @brief слот вызывающийся при прокрутке колесика мыши
     * Позволяет менять zoom изображения
     * @param event
     */
    void wheelEvent ( QWheelEvent * event ) override;
private:
    QLabel* _simpleText = nullptr;
};

#endif // GRAPHICSVIEW_H
