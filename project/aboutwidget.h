#ifndef ABOUTWIDGET_H
#define ABOUTWIDGET_H

#include <QObject>
#include <QWidget>

class QPushButton;
class QLabel;
class AboutWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AboutWidget(QPixmap icon, QWidget *parent = nullptr);

    // QWidget interface
protected:
    virtual void resizeEvent(QResizeEvent *event) override;
private:
    QLabel *programNameLabel = nullptr;
    QLabel *aboutProgramLabel = nullptr;
    QLabel *authorLabel = nullptr;
    QLabel *iconLabel = nullptr;

    QPushButton *donateButton = nullptr;
};

#endif // ABOUTWIDGET_H
