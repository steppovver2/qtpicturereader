#include "aboutwidget.h"

#include <QBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QPixmap>
#include <QSizePolicy>
#include <QSpacerItem>
#include <QPushButton>

AboutWidget::AboutWidget(QPixmap icon, QWidget *parent)
    : QWidget{parent}
{
    this->setWindowTitle("About Picture Reader");
    this->setWindowIcon(QIcon(icon));

    // Header Line
    programNameLabel= new QLabel;
    programNameLabel->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    programNameLabel->setText("Picture Reader");

    // About filed
    aboutProgramLabel= new QLabel;
    aboutProgramLabel->setAlignment(Qt::AlignTop);
    aboutProgramLabel->setWordWrap(true);
    aboutProgramLabel->setText(tr("PictureReader is a free and open-source cross-platform software"
                                  " suite for displaying, processing raster images."
                                  "\n"
                                  "\n\nPictureReader is free software as a ready-to-run "
                                  "binary distribution or as source code that you may use, "
                                  "copy, modify, and distribute in both open and "
                                  "proprietary applications."));

    // Author
    authorLabel = new QLabel();
    authorLabel->setAlignment(Qt::AlignTop);
    authorLabel->setText("Made by: Rubens Pavel (steppovver)");

    // Icon
    iconLabel = new QLabel();
    iconLabel->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    iconLabel->setPixmap(icon.scaledToWidth(200));

    // Donate
    donateButton = new QPushButton("Donate");
    donateButton->setFixedWidth(200);
    donateButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    // Spacer
    auto *verticalSpacer = new QSpacerItem(10, 10, QSizePolicy::Fixed);

    // Layout Setup
    auto *hvLayout = new QVBoxLayout;
    hvLayout->addSpacerItem(verticalSpacer);
    hvLayout->addWidget(programNameLabel);
    hvLayout->addSpacerItem(verticalSpacer);
    hvLayout->addWidget(aboutProgramLabel);
    hvLayout->addSpacerItem(verticalSpacer);
    hvLayout->addWidget(authorLabel);
    hvLayout->addSpacerItem(verticalSpacer);
    hvLayout->addWidget(donateButton);
    hvLayout->addStretch();




    // layout struct
    auto *hbLayout = new QHBoxLayout;

    hbLayout->addWidget(iconLabel);
    hbLayout->addLayout(hvLayout);

    setLayout(hbLayout);

    show();

    setFixedSize(size());
}

void AboutWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
//    iconLabel->setFixedHeight(height()/100);
//    iconLabel->setFixedWidth(width()/100);
//    iconLabel->setPixmap(icon.scaled(50,50,Qt::KeepAspectRatio));
}
