#include "image.h"
#include "mainwindow.h"
#include "dbmanager.h"
#include "mymodel.h"

#include <QImage>
#include <QDebug>
#include <QThread>



Image::Image(MainWindow *parent, const QPixmap *pixmap, DbManager *db) : QObject{parent}
{
    _parent = parent;
    _img = new QImage(pixmap->toImage());
    _hash = pixmap->cacheKey();
    if (parent)
    {
        connect(this, &Image::needToDisplayPixels,
               parent, &MainWindow::handleDisplayPixelsInTableWidget, Qt::QueuedConnection);
    }
    if (db)
    {
        connect(this, &Image::needToSavePixels,
                db, &DbManager::handleSaveDataAboutImage, Qt::QueuedConnection);
        _id = db->selectImageId(hash());
    }
}

Image::~Image()
{
    if (_img)
    {
        delete _img;
        _img = nullptr;
    }

}

qint64 Image::hash() const
{
    return _hash;
}

void Image::setId(int id)
{
    if (id > 0) {
        _id = id;
    }
}

int Image::id() const
{
    return _id;
}

void Image::countColors(bool isSaving, bool isDisplay)
{
    _rgbHash = SharedHashPointer(new QHash <QString, ColorCounter>);
    qDebug() << QThread::currentThread();

    QString tmpColorName;

    for (int x = 0; x < _img->width(); ++x)
    {
        for (int y = 0; y < _img->height(); ++y)
        {
            if (!_img->pixelColor(x, y).isValid())
                continue;
            const QColor &color = _img->pixelColor(x, y);
            auto tmp = color.name();
            auto colorCounter = _rgbHash->value(tmp, ColorCounter());
            ++colorCounter.count;

            if (tmp != tmpColorName)
                colorCounter.listOfCoordinates.append(QPair<int, int>(x, y));

            _rgbHash->insert(tmp, colorCounter);
            tmpColorName = tmp;
        }
    }
    qDebug() << "done calc";

    emit needToDisplayPixels(_rgbHash, isDisplay, true);
    if (isSaving)
        emit needToSavePixels(_rgbHash, _id);

}

void Image::setRGBHash(SharedHashPointer rgbHash)
{
    _rgbHash = rgbHash;
    emit needToDisplayPixels(_rgbHash, true, false);
}

const SharedHashPointer Image::getRGBHash() const
{
    return _rgbHash;
}


