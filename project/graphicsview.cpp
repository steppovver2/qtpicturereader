#include "graphicsview.h"

#include <QBoxLayout>
#include <QTimeLine>
#include <qevent.h>

GraphicsView::GraphicsView(QWidget  *parent) : QGraphicsView(parent)
{
    _simpleText = new QLabel(this);
    _simpleText->setAttribute(Qt::WA_TransparentForMouseEvents);
    _simpleText->setSizePolicy(QSizePolicy::MinimumExpanding , QSizePolicy::MinimumExpanding );
    _simpleText->setFrameStyle(QFrame::Panel);
    _simpleText->setWordWrap(true);
    QHBoxLayout * layout = new QHBoxLayout(this);
    layout->addWidget(_simpleText);
    layout->setAlignment(Qt::AlignCenter);
    _simpleText->setAlignment(Qt::AlignCenter);
    _simpleText->setText(tr("Choose an image by double clicking here"
                         "\nor"
                         "\ndrag it here."
                         "\nor"
                         "\npaste it from clipboard"));


}

void GraphicsView::setEnabledText(bool enable)
{
    if (enable)
        _simpleText->show();
    else
        _simpleText->hide();
}

void GraphicsView::wheelEvent(QWheelEvent *event)
{
    if(event->delta() > 0)
        scale(1.25, 1.25);
    else
        scale(0.8, 0.8);
}
