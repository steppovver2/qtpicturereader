#include "mymodel.h"

#include <QThread>
#include <QDebug>
#include <QBrush>
#include <QFont>
#include <QColor>

#define COLUMN_FIRST (tr("Color"))
#define COLUMN_SECOND (tr("Count"))

MyModel::MyModel(const QHash<QString, ColorCounter> rgbHash, QObject *parent)
    : QAbstractTableModel{parent}
{
    _rgbHash = new QHash <QString, int>;
    for (auto it = rgbHash.begin(); it != rgbHash.end(); ++it)
    {
        _rgbHash->insert(it.key(), it.value().count);
    }
}

MyModel::~MyModel()
{
    if (_rgbHash)
        delete _rgbHash;
}

int MyModel::rowCount(const QModelIndex &/*index*/) const
{
    return _rgbHash->size();
}

int MyModel::columnCount(const QModelIndex &/*index*/) const
{
    return 2;
}

QVariant MyModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (!_rgbHash) return QVariant();

    int row = index.row();
    int col = index.column();

    auto it = _rgbHash->cbegin();


    switch (role) {
    case Qt::DisplayRole:
        if (col == 0)
            return (it+row).key();
        else
            return (it+row).value();
        break;
    case Qt::ForegroundRole:
                if (col == 0) {
                    QColor color = QColor((it+row).key());
                    int yiq = ((color.red()*299)+(color.green()*587)+(color.blue()*114))/1000;
                    return (yiq >= 128) ? QColor(0,0,0) : QColor(255,255,255);
                }
        break;
    case Qt::BackgroundRole:
        if (col == 0)
            return QBrush(QColor((it+row).key()));
        break;
    case Qt::TextAlignmentRole:
        break;
    case Qt::CheckStateRole:
        break;
    }
    return QVariant();
}

QVariant MyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return QString(COLUMN_FIRST);
        case 1:
            return QString(COLUMN_SECOND);
        }
    }
    if (role == Qt::DisplayRole && orientation == Qt::Vertical)
        return section + 1;
    return QVariant();
}

