#include "graphicsscene.h"
#include <QDebug>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QUrl>
#include "mainwindow.h"

GraphicsScene::GraphicsScene(QObject *parent, MainWindow *window)
    : QGraphicsScene{parent}
{
    this->_mainWindow = window;
}

void GraphicsScene::setEnabledDoubleClick(bool enable)
{
    _isDoubleClickEnabled = enable;
}

void GraphicsScene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    event->setAccepted(false);
    qDebug() << "Scene::dragEnterEvent";
    if (!event->mimeData()->hasUrls())
        return;
    fileName = event->mimeData()->urls().first().toLocalFile();
    qDebug() <<fileName;
    qDebug() <<fileName.suffix();
    if (_fileFormatList.contains(fileName.suffix()))
    {
        event->setAccepted(true);
        event->acceptProposedAction();
    }
}

void GraphicsScene::dragMoveEvent(QGraphicsSceneDragDropEvent */*event*/)
{

}



void GraphicsScene::dropEvent(QGraphicsSceneDragDropEvent */*event*/)
{
    qDebug() << "Scene::dropEvent";
    qDebug() << fileName.absolutePath();
    _mainWindow->loadImage(fileName.filePath());
}

void GraphicsScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent */*event*/)
{
    if (_mainWindow && _isDoubleClickEnabled)
        _mainWindow->handleSelectPictureInFileDialogClicked();
}
