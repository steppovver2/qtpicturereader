#include "jsonconverter.h"

#include <QDir>
#include <QFile>
#include <QJsonObject>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>

#include "image.h"

JsonConverter::JsonConverter(QObject *parent)
    : QObject{parent}
{
}

JsonConverter::JsonConverter(const QSharedPointer<Image> image, QObject *parent) : QObject{parent}
{
    saveToJsonFile(image);
}

void JsonConverter::saveToJsonFile(const QSharedPointer<Image> image)
{
    QFile file(QDir::currentPath() + QString("%1.json").arg(image->id()));
    if(file.open(QIODevice::WriteOnly))
        qDebug() << "File open!";
    else
    {
        qCritical() << "File open error";
        return;
    }

    file.resize(0);

    QJsonObject jsonObject;

    jsonObject.insert("ImageHash", image->hash());
    jsonObject.insert("ImageId", image->id());

    QJsonArray jsonArray;
    auto rgbHash = image->getRGBHash();
    for (auto it = rgbHash->cbegin(); it != rgbHash->cend(); it++)
    {

        QJsonObject jsonObjColor;
        jsonObjColor.insert("Color", it.key());
        jsonObjColor.insert("Count", it.value().count);

        QJsonArray jsonArrayCoord;
        for (auto &el: it.value().listOfCoordinates)
        {
            QJsonArray jsonArrXY;
            jsonArrXY.append(el.first);
            jsonArrXY.append(el.second);
            jsonArrayCoord.append(jsonArrXY);
        }
        jsonObjColor.insert("Coordinates", jsonArrayCoord);
        jsonArray.append(jsonObjColor);
    }

    jsonObject.insert("Colors", jsonArray);




    QJsonDocument jsonDoc;
    jsonDoc.setObject(jsonObject);
    file.write(jsonDoc.toJson(QJsonDocument::Compact));
    file.close();
    qDebug() << "Write to file";
}
