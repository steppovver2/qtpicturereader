#ifndef TABLEWIDGET_H
#define TABLEWIDGET_H

#include <QTableWidget>

class TableWidget : public QTableWidget
{
    Q_OBJECT
public:
    TableWidget(QWidget *parent = nullptr);
    void AddItem(const QString& key,const int Col2, int i);
};

#endif // TABLEWIDGET_H
