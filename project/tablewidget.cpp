#include "tablewidget.h"
#include "qheaderview.h"

#define COLUMN_NAME1 tr("Color")
#define COLUMN_NAME2 tr("Amount")

TableWidget::TableWidget(QWidget *parent) : QTableWidget(parent)
{
    this->setColumnCount(2);
    this->setHorizontalHeaderLabels(QStringList{COLUMN_NAME1, COLUMN_NAME2});
    this->horizontalHeader()->setStretchLastSection(true);
}

void TableWidget::AddItem(const QString &keyCol1,const int Col2, int i)
{
    QColor color = QColor(keyCol1);
    int yiq = ((color.red()*299)+(color.green()*587)+(color.blue()*114))/1000;

    auto *itm = new QTableWidgetItem(tr("%1").arg(keyCol1));
    itm->setBackgroundColor(color);
    itm->setTextColor((yiq >= 128) ? QColor(0,0,0) : QColor(255,255,255));
    setItem(i, 0, itm);


    auto *itm2 = new QTableWidgetItem;
    itm2->setData(Qt::EditRole, Col2);
    setItem(i++, 1, itm2);
}
