#include "sortfilterproxymodel.h"

SortFilterProxyModel::SortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel{parent}
{

}

QVariant SortFilterProxyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation!=Qt::Vertical || role!=Qt::DisplayRole)
        return QSortFilterProxyModel::headerData(section, orientation, role);

    return section + 1;
}
