#ifndef JSONCONVERTER_H
#define JSONCONVERTER_H

#include <QObject>

class QJsonObject;
struct ColorCounter;
class Image;

/**
 * @brief Класс для обмена данными с Json файлами (создание и запись)
 */
class JsonConverter : public QObject
{
    Q_OBJECT
public:
    explicit JsonConverter(QObject *parent = nullptr);
    explicit JsonConverter(const QSharedPointer<Image> image, QObject *parent = nullptr);
    /**
     * @brief Класс для обмена данными с Json файлами (создание и запись)
     * @param image
     */
    void saveToJsonFile(const QSharedPointer<Image> image);
};

#endif // JSONCONVERTER_H
